import './App.css';
import React from 'react';
import { Table } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";

export default function UserList(props) {
    return (
        <div className="List">
            {(props.userList && props.userList.length > 0) ? 
            <Table  striped bordered hover variant="dark">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Company</th>
                </tr>
            </thead>
            {props.userList.map((user) => {
                return <React.Fragment key={user.id}>                    
                        <tbody>
                            <tr>
                                <td>{user.name}</td>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                {(user.address) ? <td>{user.address.street +" "+ user.address.suite +" "+ user.address.city +" "+
                                 user.address.zipcode}</td> : <td></td>}
                                <td>{user.phone}</td>
                                <td>{(user.company) ? user.company.name +" "+ user.company.catchPhrase : ''}</td>
                            </tr></tbody>                   
                </React.Fragment>;
            })}
            </Table> : <div>No Data!!!</div>}
        </div>
    );
}