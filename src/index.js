import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "./store/reducer";
import * as actionDispatcher from "./store/actionDispatcher";

const store = createStore(reducer, applyMiddleware(thunk));
store.dispatch(actionDispatcher.fetchUsers());
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
