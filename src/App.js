import './App.css';
import { connect } from "react-redux";
import UserList from "./UserList";
import * as actionDispatcher from "./store/actionDispatcher";
import { React, useState, useCallback } from 'react';
import debounce from 'lodash.debounce';
import { Alert } from 'react-bootstrap';




function App(props) {
  const [value, setValue] = useState('');
  //const [calldebounce, setDebounce] = useState(null);

  const onSearch = (event, props) => {
    setValue(event.target.value);
    props.load();
    setDebounce(event.target.value);
  }
  const setDebounce=useCallback(debounce((searchText) => {
    props.onSearch(searchText);
  }, 1000),[]);

  // const [timer, setTimer] = useState(null)

  // const onSearch = (e,props) => {
  //   setValue(e.target.value);
  //   props.load();   
  //   clearTimeout(timer);
  //   const newTimer = setTimeout(() => {
  //     console.log({ "debounce": e.target.value });
  //     props.onSearch(e.target.value);
  //   }, 1000);
  //   setTimer(newTimer)
  // }
  return (
    <div className="App">
      <input value={value} onChange={(e) => onSearch(e, props)}></input>
      <br />
      {(props.loading) ? <div className="Loading">Loading!!!</div> :
        (props.success) ? <UserList userList={props.userList} /> :
          <Alert variant="danger">Something Went Wrong!!!</Alert>}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    "userList": state.userList,
    "success": state.success,
    "loading": state.loading
  };
};


const mapDispachToProps = dispatch => {
  return {
    onSearch: (searchText) => dispatch(actionDispatcher.search(searchText)),
    load: () => dispatch(actionDispatcher.loading())
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);
