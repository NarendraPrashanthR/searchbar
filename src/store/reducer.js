

const initialState = {
    "userList": [],
    "success": true,
    "loading": true
};

const reducer = (state = initialState, action) => {
    try {
        const newState = { ...state, success: true };
        switch (action.type) {
            case 'INITIALLOAD':
                if (action.payload)
                    newState.userList = action.payload;
                newState.loading = false;
                break;
            case 'LOAD':
                if (action.payload) {
                    if (action.payload.filtered)
                        newState.userList = action.payload.filtered;
                }
                newState.loading = false;
                break;
            case "LOADING":
                newState.loading = true;
                newState.success = true;
                break;
            case "ERROR":
                newState.loading = false;
                newState.success = false;
                break;
            default:
                break;
        }
        return newState;
    }
    catch (err) {
        return { ...state, success: false }
    }
}

export default reducer;