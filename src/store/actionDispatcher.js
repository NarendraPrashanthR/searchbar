import axios from 'axios'

export const loading = () => {
    return {
        type: "LOADING"
    };
};

export const error = () => {
    return {
        type: "ERROR"
    };
};

export const search = val => {
    return async dispatch => {
        try {
            dispatch(loading());
            let result = await axios.get('https://jsonplaceholder.typicode.com/users');
            console.log({ result });
            let filtered = (val) ? result.data.filter((list) => list.name.toUpperCase().indexOf(val.toUpperCase()) !== -1) : result.data;
            dispatch({
                type: 'LOAD',
                payload: { filtered, searchText: val }
            });
        }
        catch (e) {
            console.log(e)
            dispatch(error());
        }
    }
};


export const fetchUsers = () => {
    return async dispatch => {
        try {
            let result = await axios.get('https://jsonplaceholder.typicode.com/users')
            console.log({ result });
            dispatch({
                type: 'INITIALLOAD',
                payload: result.data
            });
        }
        catch (e) {
            console.log(e)
            dispatch(error());
        }
    }
}